import networkx as nx
from nltk.corpus import stopwords
import nltk
from networkx.algorithms import community
import matplotlib.pyplot as plt
import logging
from random import random
import requests
import json
import boto3
from configurations import *
import os
from codeswitch.codeswitch import SentimentAnalysis

nltk.download('stopwords')
analyze_sentiment = SentimentAnalysis('spa-eng')

HOST_JAVAMS = os.environ.get('HOST_JAVAMS')

def query(payload): # {"inputs": "I like you. I love you"}
    # API_URL = "https://api-inference.huggingface.co/models/sagorsarker/codeswitch-spaeng-sentiment-analysis-lince"
    # headers = {"Authorization": "Bearer api_jcqqSjNoHPkHCQJkVcDmjvyePBbdifMYEL"}
    # data = json.dumps(payload)
    # response = requests.request("POST", API_URL, headers=headers, data=data)
    # return json.loads(response.content.decode("utf-8"))
    result = analyze_sentiment.analyze(payload['inputs'])
    value = result[0]['score']*2 - 1
    if result[0]['label'] == 'LABEL_0':
        value = - value
    return value

def load_file(filename):
    logging.info('Loading text')
    with open(filename,'r') as f:
        text = f.readlines()
    return text

def preprocess(line):
    stop_words = set(stopwords.words('spanish'))
    line = [item.lower() for item in line if not item.lower() in stop_words]
    line = [item.lower() for item in line if not item.lower() in ['mas', 'más', 'pues', 'si', 'sí', 'mi', 'tu', 'el', 'te', 'de', 'se', 'solo', 'aun', 'eh', 'mí', 'tú', 'él', 'té', 'sé', 'sólo', 'aún', 'asi', 'así', 'vez']]
    line = [item.lower() for item in line if not item.lower() in [',', '.', '-', '_', '?', '¿', '!', '¡' ]]
    return line

def create_graph(text):
    logging.info('Creating graph')
    word_list = []
    G = nx.Graph()
    for line in text:
        line = (line.strip()).split()
        line = preprocess(line)
        for i, word in enumerate(line):
            if i != len(line)-1:
                word_a = word
                word_b = line[i+1]
                if word_a not in word_list:
                    word_list.append(word_a)
                if word_b not in word_list:
                    word_list.append(word_b)
                if G.has_edge(word_a,word_b):
                    G[word_a][word_b]['weight'] += 1
                else:
                    G.add_edge(word_a,word_b, weight = 1)
    return G

def calculate_central_nodes(text_network):
    logging.info('Calculating centrality')
    bc = (nx.betweenness_centrality(text_network,weight='weight'))
    nx.set_node_attributes(text_network, bc, 'betweenness')
    bc_len = len(bc.values())
    # print(bc_len)
    # print(bc_len//10)
    bc_threshold = sorted(bc.values(), reverse=True)[bc_len//3]
    to_keep = [n for n in bc if bc[n] > bc_threshold]
    filtered_network = text_network.subgraph(to_keep)
    return filtered_network

def create_and_assign_communities(text_network):
    logging.info("Assigning communities")
    communities_generator = community.girvan_newman(text_network)
    top_level_communities = next(communities_generator)
    next_level_communities = next(communities_generator)
    communities = {}
    for community_list in next_level_communities:
        for item in community_list:
            communities[item] = next_level_communities.index(community_list)
    return communities

# def draw_final_graph(text_network,communities):
#     logging.info('Drawing')
#     pos = nx.spring_layout(text_network,scale=2)
#     color_list = []
#     color_map = []
#     community_count = max(communities.values())
#     for i in range(0,community_count+1):
#         color_list.append((random(), random(), random()))
#     for node in text_network:
#         color_index = communities[node]
#         color_map.append(color_list[color_index])
#     betweenness = nx.get_node_attributes(text_network,'betweenness')
#     betweenness = [x * 10000 for x in betweenness.values()]
    # nx.draw(text_network,with_labels=True,node_size=betweenness,font_size=8,node_color=color_map,width=4,edge_cmap=plt.cm.Blues)
    # plt.draw()
    # plt.show()

def job_analyze_text (transcription_id, transcription_url):
    text = requests.get(url = transcription_url).text
    obj = json.loads(text)
    complete_text = []
    word_times = {}
    for line in obj['transcription']:
        complete_text.append(" ".join(list(map(lambda x: x['word'], line))))
        for item in line:
            if item['word'] in word_times.keys():
                # TODO: fix key names
                word_times[item['word']].append({'start_time':item['start_time'] , 'end_time': item['end_time']})
            else:
                word_times[item['word']] = [({'start_time': item['start_time'], 'end_time': item['end_time']})]
        
    print('complete_text::',complete_text)
    clusters = analyze_text(complete_text)
    for k in clusters.keys():
        line = ""
        for el in clusters[k]:
            line += ' '+ el['word']
            el['ocurrence_times'] =  word_times[el['word']] if el['word'] in word_times.keys() else [{'start_time': 0, 'end_time': 0}]
        payload = {
            "inputs": line
        }
        polaridad = query(payload) # {"inputs": "I like you. I love you"}
        # if type(polaridad) != list and 'error' in polaridad.keys():
        #     polaridad = 0
        # else:
        #     polaridad = polaridad[0][1]['score']*2 -1
        clusters[k] = {'words': clusters[k], 'line': line, 'cluster_polarity': polaridad}
    
    # save result in s3

    job_name = transcription_id
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(results_analysis_bucket_name)
    key= 'result_text_analysis_{}.json'.format(job_name)
    aux = bucket.Object(key).put(Body=(bytes(json.dumps(clusters).encode('UTF-8'))))

    result_url = "https://results-analysis-dp2.s3.amazonaws.com/%s" % (key)
    print('bucket analysis object result_url::', result_url)

    # call service /saveanalysis {text_analysis_id, analysis_url, job_state (2)}
    try:
        # uncoment before push
        service_url = HOST_JAVAMS + save_analysis_url+'?text_analysis_id={}&analysis_url={}&job_state=1'.format(transcription_id, result_url)
        print('call service::', service_url)
        x = requests.post(service_url)
        print(x.text)
        pass
    except Exception as e:
        print(e)
    
    return clusters



def analyze_text(text):
    # text = load_file('test_text2.txt')
    text_network = create_graph(text)
    text_network = calculate_central_nodes(text_network)
    communities = create_and_assign_communities(text_network)
    weights = nx.get_node_attributes(text_network,'betweenness')
    clusters = {}
    for node in text_network.nodes:
        cluster_idx = communities[node]
        weight = weights[node]
        if cluster_idx not in clusters.keys():
            clusters[cluster_idx] = []
        clusters[cluster_idx].append({'word':node, 'weight': weight})

    for cls in clusters.keys():
        clusters[cls] = sorted(clusters[cls], key=lambda x: x['weight'])[::-1]

    # print("communities::", communities)
    # print("nodes::", text_network.nodes)
    # print("weights::", nx.get_node_attributes(text_network,'betweenness'))
    return clusters